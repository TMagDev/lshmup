﻿using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    /**
     * This file handles player input by listening to Unity's Input inputString and reading
     * each character that is passed to its buffer. When a new character comes in, we ask
     * our GameController to pick it up and process it.
     * When we type input, if we don't have a target assigned, we look for the first target
     * we can find, if there is none nothing happens, if there is one we only process input
     * towards that target from there onwards, until the target is killed.
     **/

    /**
     * Public variables
     **/
    // A reference to the prefab of the weapon shot we are going to create
    public GameObject m_WeaponShotPrefab;
    // A reference to where the weapon spawn point is
    public Transform m_WeaponSpawn;

    // The SFX we want to play when we shoot towards an enemy
    public AudioClip m_ShotClip;

    /**
     * Private variables
     **/
    // A reference to our GameController script
    private GameController m_GameControllerScript;
    // The target the player character is currently aiming at, as a GameObject
    private GameObject m_Target;
    public GameObject Target
    {
        get
        {
            return m_Target;
        }

        set
        {
            m_Target = value;

            // Store the target's Enemy script
            m_TargetEnemyScript = m_Target != null ? m_Target.GetComponent<Enemy>() : null;

            // If we are setting a target which is not null, set to the Chase target script as well
            if (m_Target != null)
                ChaseScript.Target = m_Target.transform;
        }
    }

    // We also grab a reference to the target's Enemy script so we can cache it, rather
    // then fetching it on each update
    private Enemy m_TargetEnemyScript;
    // Grab a reference to our chase script so we can rotate this player whenever we want
    private Chase m_ChaseScript;
    public Chase ChaseScript
    {
        get
        {
            return m_ChaseScript;
        }

        set
        {
            m_ChaseScript = value;
        }
    }
    // Create a new GameObject pool for our shots
    private GameObjectPooler m_PlayerShotPooler;

    // Use this for initialization
    private void Start()
    {
        // Fetch the Game Controller script using it's unique Tag
        m_GameControllerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        // Fetch our reference to the Chase script
        ChaseScript = GetComponent<Chase>();

        // Initialize our PlayerShotPooler
        m_PlayerShotPooler = new GameObjectPooler(m_WeaponShotPrefab, GameController.m_EffectsParentTransform);
    }

    // Update is called once per frame
    private void Update()
    {
        // Read each character that is passed to the inputString buffer
        foreach (char input in Input.inputString)
        {
            // The inputString buffer reads three special characters, backspace and two types of return
            // We want to avoid these scenarios, so we say only trigger our event if neither of these
            // were pressed
            if (input != "\b"[0] && input != "\n"[0] && input != "\r"[0])
            {
                // If we don't have a target, we get the first one the GameController can find matching this input
                if (m_Target == null)
                    // Tell the game controller to look for the first matching enemy
                    Target = m_GameControllerScript.FindTargetStartingWithChar(input);

                // Look at our input and see if it matches the first char in the target's healthKD
                if (m_Target != null && m_TargetEnemyScript.MatchFirstCharInHealth(input))
                {
                    // If it does, then we shoot towards the target
                    ShootTowardsTarget();

                    // And damage our target's text display
                    DamageTargetText();
                }
            }
        }
    }

    /**
     * This function shoots a projectile (WeaponShotPrefab) towards our current target.
     **/
    private void ShootTowardsTarget()
    {
        // Play our shot SFX
        SoundManager.Instance.PlaySinglePlayer(m_ShotClip);

        // Calculate in which direction you want the shot to rotate towards
        Quaternion direction = Quaternion.LookRotation(transform.forward, m_Target.transform.position - transform.position);

        // Get the next available PlayerShot from our pool
        GameObject playerShotClone = m_PlayerShotPooler.GetOrCreateAvailableGameObject();

        // If there is an available PlayerShot we can use, let's reset some data
        playerShotClone.transform.position = m_WeaponSpawn.position;
        playerShotClone.transform.rotation = direction;

        // Set the target of the Chase script of our instantiated shot to our target
        playerShotClone.GetComponent<Chase>().Target = m_Target.transform;

        // As well as the target of the PlayerShot script
        playerShotClone.GetComponent<PlayerShot>().Target = m_TargetEnemyScript;
    }

    /**
     * Here we simply remove the target's first char, and if the target's health text doesn't have
     * any chars left we can set our target to null and start looking for other targets.
     **/
    private void DamageTargetText()
    {
        // We visually remove a char from the target's Health
        // Note: But we only kill the target when the projectile hits it
        m_TargetEnemyScript.RemoveCharFromHealthText();

        // We have to check if our enemy no longer has a target HealthText here, if so, we can start looking for other targets
        if (m_TargetEnemyScript.IsHealthTextDead())
            // We can target whatever we want
            Target = null;
    }
}
