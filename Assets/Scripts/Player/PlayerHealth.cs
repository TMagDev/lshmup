﻿using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    /**
     * This files handles player health logic. 
     **/

    /**
     * Public variables
     **/
    // A reference to our player on death explosion prefab
    public GameObject m_PlayerDeathExplosionPrefab;

    /**
     * Private variables
     **/
    // Represents the player's health as an integer
    private int m_Health;
    // Grab a reference to our GameController script
    private GameController m_GameControllerScript;

    /**
     * Check for whenever a trigger collider enters the player's own collider.
     **/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If we collided with a game object whose tag is Enemy
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Destroy the game object that collided with us
            Destroy(collision.gameObject);

            // Then we destroy the player by damaging it equal to it's full health
            Damage(m_Health);
        }
    }

    // Use this for initialization
    void Start()
    {
        // Initialize the player health
        m_Health = 1;
        // Grab our only GameController
        m_GameControllerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    /**
     * This function takes an integer and reduces our player's health by
     * that amount.
     **/
    public void Damage(int damagePoints)
    {
        // Remove the points from the player's health
        m_Health -= damagePoints;

        // Check if the player is killed
        if (m_Health <= 0)
        {
            // If we died, then
            // Instantiate a clone of our player death explosion in our place
            Instantiate(m_PlayerDeathExplosionPrefab, transform.position, Quaternion.identity, 
                GameController.m_EffectsParentTransform);

            // Tell the game controller, it's game over
            m_GameControllerScript.GameOver();

            // Destroy ourselves
            Destroy(gameObject);
        }
    }
}
