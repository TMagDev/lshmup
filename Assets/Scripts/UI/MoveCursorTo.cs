﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCursorTo : MonoBehaviour
{

    /**
     * This script "moves" (read, disables this button, activates the next one) the cursor
     * to the next or previous position depending on the raw axis value of the up or down
     * key.
     * It should be attached to the panel holding ALL the cursors and then set the cursor
     * positions in the editor.
     * Also, it's important we only move the cursor when it's enabled, if it's not enabled
     * it means that something else is being done.
     **/

    /**
     * Public variables
     **/
    // This is the array of cursor positions we can move to
    public GameObject[] m_CursorPositions;

    /**
     * Private variables
     **/
    // The current position we are in
    private int m_CurrentCursorPosition;

    // Use this for initialization
    void Start()
    {
        // Initialize as 0
        m_CurrentCursorPosition = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Whenever the player presses the Vertical keys and if we have positions to move to
        if (Input.GetButtonDown("Vertical") && m_CursorPositions[m_CurrentCursorPosition].activeSelf)
            // We use GetAxisRaw so values are either -1, 0 or 1
            // Note: We also multiply by -1 because otherwise the input is reversed
            MoveCursor((int)Input.GetAxisRaw("Vertical") * -1);
    }

    /**
     * This function receives a direction and either moves the cursor
     * to the next position or to the previous position.
     * Note: "moving" here means disabling the previous position and enabling the next one.
     **/
    private void MoveCursor(int direction)
    {
        // Play the move cursor sound
        SoundManager.Instance.PlayCursorClip();

        // Disable the current position
        m_CursorPositions[m_CurrentCursorPosition].SetActive(false);

        // Move our current position to new position according to our direction
        if (direction > 0)
            // Move down
            m_CurrentCursorPosition++;
        else if (direction < 0)
            // Move up
            m_CurrentCursorPosition--;

        // Safety checks
        if (m_CurrentCursorPosition < 0)
            // Then we set to the last position in our positions
            m_CurrentCursorPosition = m_CursorPositions.Length - 1;
        else if (m_CurrentCursorPosition >= m_CursorPositions.Length)
            // Then we set to the beginning
            m_CurrentCursorPosition = 0;

        // Enable this new position
        m_CursorPositions[m_CurrentCursorPosition].SetActive(true);
    }
}
