﻿using UnityEngine;
using UnityEngine.UI;

public class UIScoreImage : MonoBehaviour
{

    /**
     * This is one of the Score Images.
     * I decided to create a small script that holds the number of this image
     * as well as the sprite to display.
     * This is easier than, say, comparing sprite names and looking for the next
     * one.
     **/

    /**
     * Public variables
     **/
    // The number of this image
    public int m_Number;

    /**
     * Private variables
     **/
    // The Unity UI Image attached
    private Image m_Image;

    // When this GameObject is enabled
    private void OnEnable()
    {
        // When we enable this UIScoreImage we want to fetch the image component as well
        m_Image = GetComponent<Image>();
    }

    // Use this for initialization
    private void Start()
    {
        // Initialize our number and grab our attached UI Image
        m_Number = 0;
    }

    /**
     * A simple function which swaps the sprites in the UI Image attached to this and updates
     * our number values.
     **/
    public void UpdateScoreImage(int newNumber, Sprite newImage)
    {
        m_Number = newNumber;
        m_Image.sprite = newImage;
    }

}
