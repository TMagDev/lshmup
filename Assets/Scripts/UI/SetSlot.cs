﻿using UnityEngine;
using UnityEngine.UI;

public class SetSlot : MonoBehaviour {

    /**
     * This script exists so that each prefab knows which one of their child input fields
     * belongs to the title and the contents. We can then add these scripts to the MenuManager,
     * and have it assign the title and contents as need be.
     **/

    /**
     * Public variables
     **/
    // The panel which holds the title input field
    public Transform m_TitlePanel;
    // The panel which holds the contents input field
    public Transform m_ContentsPanel;
    // The index of this set slot
    public int m_SetSlotIndex;

    /**
     * Private variables
     **/
    // The InputField that holds the title
    private InputField m_TitleInputField;
    public InputField TitleInputField
    {
        get
        {
            return m_TitleInputField;
        }

        set
        {
            m_TitleInputField = value;
        }
    }
    // The various InputFields that correspond to the contents
    private InputField[] m_ContentsInputField;
    public InputField[] ContentsInputField
    {
        get
        {
            return m_ContentsInputField;
        }

        set
        {
            m_ContentsInputField = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        // Grab the input field below the title
        m_TitleInputField = GetComponentInChildren<InputField>();
        // Grab all the input fields below the contents
        m_ContentsInputField = GetComponentsInChildren<InputField>();
    }
}
