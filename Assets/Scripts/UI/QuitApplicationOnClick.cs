﻿using UnityEngine;

public class QuitApplicationOnClick : MonoBehaviour
{

    /**
     * A simple script that enables UI to quit the application on click.
     **/

    /**
     * This function quits the application when running builds, and stops the editor when programming.
     **/
    public void QuitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
