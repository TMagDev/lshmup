﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveDataOnBack : MonoBehaviour
{

    /**
     * Because Unity can't set up onClick events for a button in the editor, I had to create
     * a script which binds a function to the onClick event itself, with the proper parameters.
     **/

    /**
     * Private variables
     **/
    // A reference to the button we are bound to
    private Button m_Button;
    // The SetSlot script is what holds the title and contents of the input fields
    private SetSlot m_ParentSetSlot;
    // We also need a reference to our menu controller
    private MenuController m_MenuController;

    // Use this for initialization
    void Start()
    {
        // Get our button
        m_Button = GetComponent<Button>();
        // Get our parent set slot
        m_ParentSetSlot = GetComponentInParent<SetSlot>();
        // Get our menu controller
        m_MenuController = GameObject.FindGameObjectWithTag("MenuController").GetComponent<MenuController>();

        // Add the CallSaveSet function to the onClick event
        m_Button.onClick.AddListener(CallSaveSet);
    }

    /**
     * This function gets the contents and name of our associated set slot and passes both of them
     * to our FileManager so we can save locally.
     **/
    private void CallSaveSet()
    {
        // Store the name of the set
        string setName = m_ParentSetSlot.TitleInputField.text;

        // If the name is empty, don't save
        if (setName.Length <= 0)
            return;

        // Make an array of strings, equal in length to the amount of input fields we have
        string[] contents = new string[m_ParentSetSlot.ContentsInputField.Length - 1];

        // Count the empty input fields
        int emptyInputFields = 0;

        // Now for each input field, add it to the array
        // Note: We start at 1 because for some reason GetComponentsInChildren is giving us the title input field as well
        for (int i = 0; i < contents.Length; i++)
        {
            // Set the text of the input field
            contents[i] = m_ParentSetSlot.ContentsInputField[i + 1].text;

            // If this input field is empty
            if (String.IsNullOrEmpty(contents[i]))
                emptyInputFields++;
        }

        // If all input fields are empty, do nothing
        if (emptyInputFields == contents.Length)
            return;

        // If we have a file in this slot, then use it's highscore, otherwise use 0
        int highscore = FileManager.Instance.SetsData[m_ParentSetSlot.m_SetSlotIndex] == null ? 0 : FileManager.Instance.SetsData[m_ParentSetSlot.m_SetSlotIndex].Highscore;

        // Call our file manager to save this set's contents
        FileManager.Instance.SaveSetAtIndex(m_ParentSetSlot.m_SetSlotIndex, m_ParentSetSlot.TitleInputField.text, contents,
            highscore);

        // Tell our menu controller to reload the sets
        m_MenuController.AskFileManagerToLoadSets();
    }
}
