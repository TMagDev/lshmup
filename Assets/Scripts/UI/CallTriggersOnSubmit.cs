﻿using UnityEngine;
using UnityEngine.UI;

public class CallTriggersOnSubmit : MonoBehaviour
{

    /**
     * This script is added to our blinking cursor prefab so that we can trigger
     * specific events when the player hits the "Submit" key.
     **/

    /**
     * Public variables
     **/
    // Which button are we gonna trigger the OnClick event
    public Button m_ButtonToTrigger;
    // Which input field we want to focus and select
    public InputField m_InputFieldToFocus;

    // Update is called once per frame
    void Update()
    {
        // When the player hits the "Submit" key, call triggers
        if (Input.GetButtonDown("Submit"))
            CallTriggers();
    }

    /**
     * Sequentially call each one of the triggers we want, if we have the associated
     * UI element assigned.
     **/
    private void CallTriggers()
    {
        // If we have a button
        if (m_ButtonToTrigger != null)
            // Call the onClick event
            m_ButtonToTrigger.onClick.Invoke();

        // If we have an input field
        if (m_InputFieldToFocus != null)
        {
            // Disable this cursor
            gameObject.SetActive(false);

            // Select and activate it
            m_InputFieldToFocus.ActivateInputField();
        }
    }
}
