﻿using UnityEngine;

public class PlayerShot : MonoBehaviour
{

    /**
     *  This script is what handles the trigger collisions for the player's shot.
     **/

    /**
     * Public variables
     **/
    // The prefab of the hit effect we want to trigger when this player shot hits and is destroyed
    public GameObject m_PlayerShotHitEffectPrefab;

    /**
     * Private variables
     **/
    // The target this player shot is after
    private Enemy m_Target;
    public Enemy Target
    {
        get
        {
            return m_Target;
        }

        set
        {
            m_Target = value;
        }
    }

    /**
     * Track when we enter triggers.
     **/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // When we collide with an enemy AND it's our target
        // Note: This strategy relies on the fact that, at all times, there are never two enemies
        // with the same OriginalHealthText
        if (collision.CompareTag("Enemy") && collision.GetComponent<Enemy>() != null &&
            Target.OriginalHealthText.Equals(collision.GetComponent<Enemy>().OriginalHealthText))
        {
            // Damage our target
            Target.Damage();

            // When this player shot is destroyed, we want to instantiate the player shot hit effect
            Instantiate(m_PlayerShotHitEffectPrefab, transform.position, Quaternion.identity,
                GameController.m_EffectsParentTransform);

            // And destroy ourselves
            Destroy();
        }
    }

    /**
     * Because we want to object pool our shots, we don't actually destroy this game object
     * but rather deactivate it.
     **/
    private void Destroy()
    {
        // Deactivate this game object
        gameObject.SetActive(false);
    }
}
