﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour
{

    /**
     * This script is just a snippet of code that allows the holding game object to move and rotate
     * towards a given target.
     **/

    /**
     * Public variables
     **/
    // These booleans allow us to toggle in the inspector whether we want to move, rotate, or both
    public bool m_ToRotate, m_ToMove;
    // At which speed we are moving towards target
    // Note: That this is public so we can set the velocity in the inspector
    public float m_MoveSpeed;
    // At which speed we rotate this game object
    // Note: That this is public so we can set the velocity in the inspector
    public float m_RotationSpeed = 1f;

    /**
     * Private variables
     **/
    // Keep a reference to the transform of our target
    public Transform m_Target;
    public Transform Target
    {
        get
        {
            return m_Target;
        }

        set
        {
            m_Target = value;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Using Unity's physics we can use the function MoveTowards to move towards the target
        // and the function LookRotation to rotate towards the target

        // Check if our target exists
        if (Target)
        {
            // If we want to move
            if (m_ToMove)
                // Move towards the target
                MoveTowardsTarget();

            // If we want to rotate
            if (m_ToRotate)
                // Rotate towards the player
                RotateTowardsTarget();
        }
    }

    /**
     * Defines the first part of the basic behavior of our enemies which is nothing more than moving
     * towards the target.
     **/
    private void MoveTowardsTarget()
    {
        // Move towards the target, if it exists
        transform.position = Vector2.MoveTowards(transform.position, Target.position, Time.deltaTime * m_MoveSpeed);
    }

    /**
     * Defines the second part of the basic behavior of our enemies which is nothing more than rotating
     * towards the target.
     **/
    private void RotateTowardsTarget()
    {
        // We find the direction in which we want to rotate towards
        Quaternion direction = Quaternion.LookRotation(transform.forward, Target.position - transform.position);

        // Then we smoothly lerp the current rotation towards the target's direction
        transform.rotation = Quaternion.Slerp(transform.rotation, direction, m_RotationSpeed * Time.deltaTime);
    }
}
