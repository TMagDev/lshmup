﻿using UnityEngine;

public class Move : MonoBehaviour
{
    /**
     * This script enables any game object to move at a given speed towards a given direction.
     * Optionally, we may also repeat it (set it back at the top of the camera) when we reach
     * it's length.
     * Note: We don't use this for enemy characters because enemy characters move towards the
     * player character.
     **/

    /**
     * Public variables
     **/
    // At which speeds does the game object move
    // Note: Using a Vector2 to define speed in any of the 4 directions
    public Vector2 m_Speed;
    // Whether or not we want to repeat this object when we go outside the camera height
    public bool m_IsRepeatable;
    // The length of the transform we want to repeat
    // Note: We use this in case the transform is bigger than the camera height so we don't
    // reset at the camera height but rather when we reach the transform's height
    public float m_GameObjectLength;
    // In what direction are we going to be moving towards
    public Vector2 m_Direction;

    /**
     * Private variables
     */
    // Store the camera's height to avoid extra calculations
    private float m_MainCameraHeight;
    // We also check where the reset position is going to be in case some elements are already
    // in the view (even though they belong to a background out of view)
    private float m_ResetPosition;

    // Use this for initialization
    void Start()
    {
        // Calculate the main camera's height
        m_MainCameraHeight = Camera.main.orthographicSize * 2f;

        // Safety checks
        // Ensure that our m_gameObjectLength is not 0 or negative when we want to repeat this
        // game object as this is not valid
        if(m_IsRepeatable && m_GameObjectLength <= 0)
        {
            throw new UnityException("Invalid game object length " + m_GameObjectLength + ".");
        }
    }

    // Update is called once per frame
    void Update()
    {
        /**
         * We want to move the background a given set of coordinates based on time, the scroll speed, and then
         * repeat it when it reaches the length of the background.
         **/

        // By how much the background needs to move to based on our scroll speed and deltaTime
        // Note: We could have a direction Vector to check which direction the background scrolls to, but this is fine
        Vector3 movement = new Vector3(
            m_Speed.x * m_Direction.x,
            m_Speed.y * m_Direction.y,
            0) * Time.deltaTime;

        // Move the background
        transform.Translate(movement);

        // Attempt to reset at the end of the cycle again
        ResetOrDestroy();
    }

    /**
     * Checks if this game object is isRepeatable and if so, whether or not we have scrolled past
     * our background length, if we did we reset our transform position.
     * If we are not a isRepeatable object and we passed our length, we destroy this game object
     * because it has gone out of view.
     **/
    private void ResetOrDestroy()
    {
        // If we have reached our length
        if(transform.position.y <= -m_GameObjectLength)
        {
            // Check if we are to repeat
            if (m_IsRepeatable)
            {
                // Then we place ourselves at start position + camera's height
                transform.position = new Vector3(transform.position.x,
                    m_MainCameraHeight,
                    0);
            } else
            {
                // If we went past our length and we are not a isRepeatable object, destroy it
                Destroy(gameObject);
            }
        }
    }
}
