﻿using UnityEngine;

public class DestroyOnAnimationEnd : MonoBehaviour
{
    /**
     * This script contains one single function which destroy this game object
     * when the animation finishes.
     * Using Animation Events, we can put an event at the end of the animation so
     * we destroy this game object right at the last frame.
     **/
    public void DestroyAfterAnimation()
    {
        Destroy(gameObject);
    }
}
