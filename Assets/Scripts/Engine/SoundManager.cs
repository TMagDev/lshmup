﻿using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    /**
     * This script is the manager that handles the sound our game is going to output.
     * There are two AudioSources, one for the background music and another one for the SFX.
     * This manager is also a singleton, since we don't want it to be destroyed on load, and
     * we want both scenes to be able to change the currently playing audio on the sources as needed.
     **/

    /**
     * Public variables
     **/
    // The AudioSources for the background music, sound effects, and player sound effects, respectively
    public AudioSource m_BGMSource;
    public AudioSource m_SFXSource;
    public AudioSource m_PlayerSource;

    // This is the SFX we want to play when the cursor moves
    public AudioClip m_CursorSFXClip;

    // The static reference to this script
    public static SoundManager Instance;

    // Initialize variables that other scripts depend of here
    void Awake()
    {
        // If we don't have a reference, set to this
        if (Instance == null)
        {
            // Tell Unity that we don't want to destroy this GameObject when loading a new scene
            DontDestroyOnLoad(gameObject);
            // Set our instance
            Instance = this;
        }
        else if (Instance != this)
        {
            // If we are instantiating a difference reference, destroy it
            Destroy(gameObject);
        }
    }

    /**
     * This function plays a clip only once in our SFX audio source.
     **/
    public void PlaySingle(AudioClip clip)
    {
        // Set the clip in our SFX audio source
        m_SFXSource.clip = clip;
        // Play it
        m_SFXSource.Play();
    }

    /**
     * This function plays a clip only once in our Player source clip.
     * It should be called by the player for it's own SFX.
     **/
    public void PlaySinglePlayer(AudioClip clip)
    {
        // Set the clip in our SFX audio source
        m_PlayerSource.clip = clip;
        // Play it
        m_PlayerSource.Play();
    }

    /**
     * This function simply changes our background music.
     **/
    public void PlayBGM(AudioClip clip)
    {
        // If the clip already being played is the same than what we want, ignore it
        if (m_BGMSource.clip == clip)
            return;

        // Set the clip in our BGM audio source
        m_BGMSource.clip = clip;
        // Play it
        m_BGMSource.Play();
    }

    /**
     * Play the move cursor SFX.
     **/
    public void PlayCursorClip()
    {
        // Set the clip in our SFX audio source
        m_SFXSource.clip = m_CursorSFXClip;
        // Play it
        m_SFXSource.Play();
    }
}
