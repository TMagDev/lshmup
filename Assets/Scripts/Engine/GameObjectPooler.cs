﻿using UnityEngine;

public class GameObjectPooler
{

    /**
     * This is my custom implementation of a GameObject pooler.
     **/

    /**
     * Private variables
     **/
    // This is the prefab of the object we are gonna pool
    private GameObject m_GameObjectPrefab;
    // This is our pool of game objects
    private GameObject[] m_GameObjectPool;
    // How many objects are we going to pool from the start
    private int m_MinPoolAmount;
    // What's the maximum objects we can grow to
    private int m_MaxPoolAmount;
    // What's the transform we want to hook these GameObjects to
    private Transform m_ParentTransform;

    // Constructor
    public GameObjectPooler(GameObject newGameObjectPrefab, Transform parentTransform)
    {
        // Set the variables
        m_GameObjectPrefab = newGameObjectPrefab;
        m_ParentTransform = parentTransform;

        // Set our min and max pool amount
        m_MinPoolAmount = 1;
        // Note: We use 45 because that's the larger English word
        m_MaxPoolAmount = 45;

        // Initialize our GameObjectPool to the minimum we have
        m_GameObjectPool = new GameObject[m_MinPoolAmount];

        // Fill the pool with our GameObjectPrefab
        for (int i = 0; i < m_GameObjectPool.Length; i++)
        {
            // Instantiate a clone of the PlayerShot
            GameObject clone = MonoBehaviour.Instantiate(m_GameObjectPrefab, parentTransform);

            // We immediately deactivate it
            clone.SetActive(false);

            // And we add it to the pool
            m_GameObjectPool[i] = clone;
        }
    }

    /**
     * Simply clones the pool and resizes it by 1.
     **/
    private void ResizePool()
    {
        // Make a new pool with length 1 bigger
        GameObject[] biggerPool = new GameObject[m_GameObjectPool.Length + 1];

        // Copy the pool
        for (int i = 0; i < m_GameObjectPool.Length; i++)
        {
            biggerPool[i] = m_GameObjectPool[i];
        }

        // Set the new bigger pool to our pool
        m_GameObjectPool = biggerPool;
    }

    /**
     * Fetches the next available game object in the pool, or creates a new game object
     * and adds it to the pool, if we are able and need to do so.
     **/
    public GameObject GetOrCreateAvailableGameObject()
    {
        // Look for the next available PlayerShot clone we have
        for (int i = 0; i < m_GameObjectPool.Length; i++)
        {
            // If this PlayerShot in the pool is not active
            if (!m_GameObjectPool[i].activeInHierarchy)
            {
                // Activate this game object
                m_GameObjectPool[i].SetActive(true);

                // Return this available GameObject
                return m_GameObjectPool[i];
            }
        }

        // If there isn't any available and we can grow, then we resize our array and return
        // the new one
        if(m_GameObjectPool.Length < m_MaxPoolAmount)
        {
            // Resize the pool
            ResizePool();

            // Instantiate a clone of the PlayerShot
            GameObject clone = MonoBehaviour.Instantiate(m_GameObjectPrefab, m_ParentTransform);

            // And we add it to the pool at the end
            m_GameObjectPool[m_GameObjectPool.Length - 1] = clone;

            // And finally return it
            return clone;
        }

        // Else, we return null
        return null;
    }

    /**
     * This function is responsible for destroying all of the pool.
     **/
    public void DestroyPool()
    {
        // Iterate our pool and destroy our objects, one by one
        for (int i = 0; i < m_GameObjectPool.Length; i++)
        {
            // Use the MonoBehaviour to destroy the game object
            MonoBehaviour.Destroy(m_GameObjectPool[i].gameObject);
        }
    }
}
