﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class FileManager : MonoBehaviour
{

    /**
     * This is the script that is responsible for serializing and deserializing all the data
     * that the user, uses to edit and create new levels.
     * This object should not be destroyed on load so we can use it on the level scene, to load
     * the enemies's health.
     * 
     * I decided to make this script a singleton because it's the most convenient way to access
     * it, even after switching between scenes. If we had different types of file we want to
     * save in, I would choose to write the File Manager using the Service Provider pattern
     * instead.
     **/

    /**
     * Public variables
     **/
    // The static reference to this script
    public static FileManager Instance;

    /**
     * Private variables
     **/
    // Store the set data internally
    private SetData[] m_SetsData;
    public SetData[] SetsData
    {
        get
        {
            return m_SetsData;
        }

        set
        {
            m_SetsData = value;
        }
    }
    // The index of the active data set we are going to use
    private int m_ActiveSetDataIndex;
    public int ActiveSetDataIndex
    {
        get
        {
            return m_ActiveSetDataIndex;
        }

        set
        {
            m_ActiveSetDataIndex = value;
        }
    }

    // The first initialization call made for this script
    void Awake()
    {
        // If we don't have a reference, set to this
        if (Instance == null)
        {
            // Tell Unity that we don't want to destroy this GameObject when loading a new scene
            DontDestroyOnLoad(gameObject);
            // Set our instance
            Instance = this;
        }
        else if (Instance != this)
        {
            // If we are instantiating a difference reference, destroy it
            Destroy(gameObject);
        }
    }

    /**
     * This function writes a binary file to the platforms persistent data path, enabling us to save
     * set data.
     * Note: That this doesn't work for web players.
     **/
    public void SaveSetAtIndex(int index, string setName, string[] content, int highscore)
    {
        // If set exists at this index delete it
        if (SetDataExistsAtIndex(index))
            DeleteSet(index);

        // Store a variable for our persistent data path rather than calling the method over and over
        string persistentDataPath = Application.persistentDataPath;

        // Using a binary formatter, we can serialize classes in to binary files
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        // Create a new file at the persistent data path, using a FileStream
        FileStream fileStream = File.Create(Path.Combine(persistentDataPath, setName + ".data"));

        // Create a new reference to our serializable class
        SetData setData = new SetData(index, setName, content, highscore);

        // Serialize this set data
        binaryFormatter.Serialize(fileStream, setData);

        // Close our file stream
        fileStream.Close();
    }

    /**
     * This function looks to see if there are any sets in the persistent data path, and if so
     * loads them in to the correct slots.
     **/
    public SetData[] LoadSets()
    {
        // Get the file paths stored in our persistent data path, that have extension ".data"
        FileInfo[] files = Directory.GetFiles(Application.persistentDataPath, "*.data")
            .Select(f => new FileInfo(f))
            .OrderBy(f => f.CreationTime)
            .ToArray();

        // If we do have files
        SetsData = new SetData[3];

        // If we don't have any files, then do nothing
        if (files.Length <= 0)
            return null;

        // Create a binary formatter
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        // Create a file stream
        FileStream fileStream;

        // If we do have files, for each of these
        for (int i = 0; i < files.Length; i++)
        {
            // If we are analyzing more than the third slot (index 2), then we break
            if (i >= 3)
                break;

            // Open this file at this file path
            fileStream = files[i].Open(FileMode.Open);

            // Deserialize the data
            SetData deserializedSet = (SetData)binaryFormatter.Deserialize(fileStream);

            // Store the deserialized set at its corresponding index
            SetsData[deserializedSet.Index] = deserializedSet;

            // Close the stream
            fileStream.Close();
        }

        // Return the sets data as well
        return SetsData;
    }

    /**
     * This function deletes the data set at the index 0, as well as deleting the local file
     * with the same name.
     **/
    public void DeleteSet(int index)
    {
        // If the sets at this index are empty, do nothing
        if (SetsData[index] == null)
            return;

        // Delete the set locally as a file
        File.Delete(Path.Combine(Application.persistentDataPath, SetsData[index].Title + ".data"));

        // Set this set data as null
        SetsData[index] = null;
    }

    /**
     * Determines whether a SetData at the given index was loaded
     **/
    public bool SetDataExistsAtIndex(int index)
    {
        // If we are looking for an index less than 0 or bigger than 3 it doesnt exist
        if (index < 0 || index >= 3)
            return false;

        // Return if this set data is null or not
        return SetsData[index] != null;
    }
}
