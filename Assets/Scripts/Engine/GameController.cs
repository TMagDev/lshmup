﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    /**
     * The game controller is responsible for handling the state of a round. It loads language sets,
     * spawns waves, and tracks scores.
     **/

    /**
     * Public variables
     **/
    // These are the enemy prefabs from which we can choose to spawn enemies from
    public GameObject[] m_EnemiesPrefabs;
    // For simplicity, target a parent transform that we can place our enemies in to
    public Transform m_EnemiesParentTransform;

    // How long we want to wait between spawning enemies
    public float[] m_EnemySpawnWaitTime;
    // This defines a breathing room for the player to wait between rounds
    public float m_RoundWaitTime;

    // Keep an array of the numbers 0-9 represented as sprite assets
    public Sprite[] m_NumericAssets;
    // The transform of the UI Panel that holds the score visible while playing in the level
    public Transform m_UIScorePanel;
    // The UI Panel to display when the player wins the game
    public GameObject m_UIGameWonPanel;
    // The transform of the UI Panel that holds the current score, and the second UI panel that holds the highscore
    public Transform[] m_UICurrentScorePanel;
    public Transform m_UIHighscorePanel;
    // The UI Panel to display when the player loses the game
    public GameObject m_UIGameLossPanel;

    // The BGM we want to play when we load our level
    public AudioClip m_LevelBGMClip;
    // The clip we want to play when the player dies
    public AudioClip m_GameOverClip;
    // The clip we want to play when the player wins
    public AudioClip m_VictoryClip;

    // The parent transform where we place effects on
    // Note: I made this variable static because all GameObject should know which is this transform
    public static Transform m_EffectsParentTransform;

    /**
     * Private variables
     **/
    // Get a reference to our player's PlayerInput script
    private PlayerInput m_PlayerInputScript;

    // Calculate where we are going to spawn the enemies
    private Vector3 m_SpawnPosition;

    // Track how many rounds we are in so we can add logarithmic progression to the enemy spawning process
    private int m_Rounds;

    // This is an array of strings representing each word loaded from the language set file
    private List<string> m_LanguageSet;

    // A list of enemies that are in the current round
    private List<Enemy> m_EnemiesInRound;
    // How many enemies are left to destroy
    // Note: I opted to have an int holding this number, so every time an enemy is destroyed we don't have
    // to iterate through the m_EnemiesInRound array
    private int m_EnemiesLeftInRound;
    // How many enemies have we destroyed in total for the whole level
    private int m_EnemiesDestroyedInLevel;
    // How many enemies do we need to destroy in all of the level
    private int m_EnemiesNeededToWin;

    // Track the player's current score
    private int m_Score;
    // Keep the highscore of the level
    private int m_Highscore;

    // The boss prefab we want to instantiate at the end
    private List<string> m_Bosses;

    // Use this to initialize references that are needed by other scripts
    private void Awake()
    {
        // Grab the GameObject which is the effects transform
        m_EffectsParentTransform = GameObject.FindGameObjectWithTag("Effects").transform;
    }

    // Use this for initialization
    private void Start()
    {
        // Hide and lock our cursor, if we are not on the editor
#if UNITY_EDITOR
        Cursor.lockState = CursorLockMode.None;
#else
        Cursor.lockState = CursorLockMode.Locked;
#endif
        Cursor.visible = false;

        // Initialize our lists
        m_LanguageSet = new List<string>();
        m_Bosses = new List<string>();

        // Load the language set from a file
        string[] contents;

        // If our file manager is null
        if (FileManager.Instance == null)
        {
            contents = new string[1] { "All your base are belong to us" };
            m_Highscore = 0;
        }
        else
        {
            // Load the sets again
            FileManager.Instance.LoadSets();

            // Else we load from our file manager
            contents = FileManager.Instance.SetsData[FileManager.Instance.ActiveSetDataIndex].Contents;
            // Load the highscore from the file as well
            m_Highscore = FileManager.Instance.SetsData[FileManager.Instance.ActiveSetDataIndex].Highscore;
        }

        // Add the contents to the language set
        for (int i = 0; i < contents.Length; i++)
        {
            // If the content at this index is empty, skip it
            if (String.IsNullOrEmpty(contents[i]))
                continue;

            // Check if the health is a sentence by checking if it has a whitespace
            // Note: That by doing this way I guarantee that I can instantiate the bosses at the end of the level
            if (contents[i].Any(s => Char.IsWhiteSpace(s)))
            {
                // If it contains a whitespace, it's a sentence, add this health as a boss
                m_Bosses.Add(contents[i]);
            }
            else
            {
                // Add it to the language set
                m_LanguageSet.Add(contents[i]);
            }
        }

        // Set how many enemies we need to destroy to win the level to the combined size of our language set
        // and bosses
        m_EnemiesNeededToWin = m_Bosses.Count + m_LanguageSet.Count;

        // Start our rounds with 0, this is then incremented to 2 in the StartRound() so we ensure we
        // have a starting round with 1 enemy
        m_Rounds = 0;

        // Initially we haven't destroyed any enemies in the level either
        m_EnemiesDestroyedInLevel = 0;

        // Our player's score also starts as 0
        m_Score = 0;

        // Find our player
        m_PlayerInputScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInput>();

        // Tell our SoundManager to play the level's BGM
        SoundManager.Instance.PlayBGM(m_LevelBGMClip);

        // Spawn our enemy waves
        StartCoroutine(StartRound());
    }

    /**
     * This coroutine is responsible for spawning enemy waves in a set interval.
     **/
    private IEnumerator StartRound()
    {
        // Wait a few seconds before starting the round
        yield return new WaitForSeconds(m_RoundWaitTime);

        // Increment our rounds
        m_Rounds++;

        // If our language set has enemies to spawn, spawn a round for them
        if (m_LanguageSet.Count > 0)
            StartCoroutine(SpawnBasicEnemies());
        else
            SpawnBoss();
    }

    /**
     * This coroutine launched from the StartRound() spawns basics enemies from the language set.
     **/
    private IEnumerator SpawnBasicEnemies()
    {
        // How many enemies we are gonna spawn this round
        // Note: We try to do a linear progression to a cap of X
        int enemiesToSpawn = m_Rounds;

        // Ensure that we never spawn more enemies than we can by only instantiating how many we have left
        // if we are trying to spawn more than that
        int enemiesLeftToSpawn = m_LanguageSet.Count;
        if (m_Rounds > enemiesLeftToSpawn)
            enemiesToSpawn = enemiesLeftToSpawn;

        // Declare our new array of how many enemies are there in the round
        m_EnemiesInRound = new List<Enemy>();
        // Keep track how many enemies the player needs to destroy in the round
        m_EnemiesLeftInRound = enemiesToSpawn;

        // Now loop our enemiesToSpawn and spawn them
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            // Declare a GameObject of the prefab we are going to use
            GameObject prefabToInstantiate;
            // The health we are going to use for this enemy
            string healthToUse;

            // Our spawn position is always going to be a random position at the top limits of the camera
            m_SpawnPosition = new Vector3(UnityEngine.Random.Range(-4.5f, 4.5f), 6f, 0);

            // Randomly pick words off the set
            int randomIndex = UnityEngine.Random.Range(0, m_LanguageSet.Count - 1);

            // Set the health that we want to use
            healthToUse = m_LanguageSet[randomIndex];

            // Check if the health is not a letter
            if (healthToUse.Length > 1)
            {
                // If it doesn't, it's a word, instantiate a Word enemy
                prefabToInstantiate = m_EnemiesPrefabs[1];
            }
            else
            {
                // The health is a letter, so instantiate the Letter enemy
                prefabToInstantiate = m_EnemiesPrefabs[0];
            }

            // Remove this enemy from our language set
            m_LanguageSet.RemoveAt(randomIndex);

            // Spawn a new enemy
            SpawnEnemy(prefabToInstantiate, healthToUse, m_SpawnPosition, prefabToInstantiate.transform.rotation);

            // Wait enough time before spawning the next enemy
            // We randomize between a range so some enemies spawn faster than others
            yield return new WaitForSeconds(UnityEngine.Random.Range(m_EnemySpawnWaitTime[0], m_EnemySpawnWaitTime[1]));
        }
    }

    /**
     * This coroutine is responsible for spawning bosses.
     **/
    private void SpawnBoss()
    {
        // How many enemies we are gonna spawn this round
        // Note: We try to do a linear progression to a cap of X
        int enemiesToSpawn = m_Rounds;

        // Ensure that we never spawn more enemies than we can by only instantiating how many we have left
        // if we are trying to spawn more than that
        int enemiesLeftToSpawn = m_LanguageSet.Count;
        if (m_Rounds > enemiesLeftToSpawn)
            enemiesToSpawn = enemiesLeftToSpawn;

        // Declare our new array of how many enemies are there in the round
        m_EnemiesInRound = new List<Enemy>();
        // Keep track how many enemies the player needs to destroy in the round
        m_EnemiesLeftInRound = enemiesToSpawn;
        // The health we are going to use for this enemy
        string healthToUse;

        // Our spawn position is always going to be a random position at the top limits of the camera
        // Note: That for bosses we spawn them just a bit higher because of their sprite size
        m_SpawnPosition = new Vector3(UnityEngine.Random.Range(-4.5f, 4.5f), 6.8f, 0);

        // Set the health we want to use to this index
        healthToUse = m_Bosses[0];

        // Remove this boss from our boss list
        m_Bosses.RemoveAt(0);

        // Say that we only want to spawn one enemy
        m_EnemiesLeftInRound = 1;

        // Spawn a new enemy
        SpawnEnemy(m_EnemiesPrefabs[2], healthToUse, m_SpawnPosition, m_EnemiesPrefabs[2].transform.rotation);
    }

    /**
     * Adds score for the player, when an enemy is killed.
     **/
    private void Score(int points)
    {
        // Increment to our current score
        m_Score += points;

        // Update our level score
        MatchTransformWithScore(m_UIScorePanel, m_Score);
    }

    /**
     * This function takes a transform and updates its UI Score Images so they
     * match the given score.
     **/
    private void MatchTransformWithScore(Transform targetTransform, int score)
    {
        // Get all the components in children that have UI Score Image scripts
        UIScoreImage[] uiScoreImages = targetTransform.GetComponentsInChildren<UIScoreImage>();

        // Track our iterations in reverse
        int reverseCounter = uiScoreImages.Length - 1;

        // Update our UI by iterating each Image in our UI Score panel in reverse,
        // and then ensuring our UI Score images match the numbers
        // Note: We are using a while loop instead of a for loop so we can iterate
        // only through the necessary digits
        while (score != 0)
        {
            // Get the leftover of the division of local score by 10 so we can read the single digit
            int leftover = score % 10;

            // Get the UI Score Image script at this index
            UIScoreImage uiScoreImageScript = uiScoreImages[reverseCounter];

            // Ensure that the image at this index is matching
            if (uiScoreImageScript.m_Number != leftover)
            {
                // Then set it's new number and sprite (by grabbing the sprite at this leftover index)
                uiScoreImageScript.UpdateScoreImage(leftover, m_NumericAssets[leftover]);
            }

            // Divide our local score so we iterate the next number
            score /= 10;

            // Decrement our reverse counter
            reverseCounter--;
        }
    }

    /**
     * Called when the player wins the game.
     **/
    private void GameWon()
    {
        // Play our victory clip
        SoundManager.Instance.PlayBGM(m_VictoryClip);

        // Enable the Game Won panel
        m_UIGameWonPanel.SetActive(true);

        // Match the highscore with what's on file
        MatchTransformWithScore(m_UIHighscorePanel.transform, m_Highscore);

        // If our current score is bigger than the highscore, set that as the new highscore
        if (m_Score >= m_Highscore)
        {
            // Set the new score as our highscore
            m_Highscore = m_Score;

            // If we have a file manager
            if (FileManager.Instance != null)
            {
                // Get the active set data
                SetData activeSetData = FileManager.Instance.SetsData[FileManager.Instance.ActiveSetDataIndex];

                // Save the set with the new highscore
                FileManager.Instance.SaveSetAtIndex(activeSetData.Index, activeSetData.Title, activeSetData.Contents,
                    m_Highscore);
            }
        }

        // Update the score UI at our current score panel at the won panel
        MatchTransformWithScore(m_UICurrentScorePanel[0].transform, m_Score);
    }

    /**
     * Is called when the player is destroyed by one of the ships.
     **/
    public void GameOver()
    {
        // Play the game over song
        SoundManager.Instance.PlayBGM(m_GameOverClip);

        // Destroy all the enemies
        for (int i = 0; i < m_EnemiesInRound.Count; i++)
        {
            // If it's not empty
            if (m_EnemiesInRound[i] != null)
                Destroy(m_EnemiesInRound[i].gameObject);
        }

        // Display the game loss panel
        m_UIGameLossPanel.SetActive(true);

        // Update the score UI at our current score panel at the loss panel
        MatchTransformWithScore(m_UICurrentScorePanel[1].transform, m_Score);
    }

    /**
     * Handles when an enemy is destroyed by the player.
     **/
    public void EnemyIsDead(int index)
    {
        // We might have to reset our BGM, so attempt to do so
        SoundManager.Instance.PlayBGM(m_LevelBGMClip);

        // Handle score
        Score(m_EnemiesInRound[index].PointsWorth);

        // Then Destroy this game object
        Destroy(m_EnemiesInRound[index].gameObject);

        // Set this enemy in our array to null
        m_EnemiesInRound[index] = null;

        // Set the player chase target to null
        m_PlayerInputScript.ChaseScript.Target = null;

        // Increment the amount of enemies destroyed in this level
        m_EnemiesDestroyedInLevel++;

        // Decrement how many enemies are left to destroy in this round
        m_EnemiesLeftInRound--;

        // If we destroyed enemies in the level equal to the size of our language set
        if (m_EnemiesDestroyedInLevel >= m_EnemiesNeededToWin)
            // Then our player won
            GameWon();
        else if (m_EnemiesLeftInRound <= 0)
            // If we can still spawn more enemies
            // Then start a new round
            StartCoroutine(StartRound());
    }

    /**
     * This function is called by the Player game object to find an enemy target which
     * starts with a given char. This begins the Player's writing slaughter process.
     **/
    public GameObject FindTargetStartingWithChar(char input)
    {
        // If our m_EnemiesInRound is null or empty, then return null
        if (m_EnemiesInRound == null || m_EnemiesInRound.Count <= 0)
            return null;

        // Search each enemy for the one whose first character matches the player's input
        for (int i = 0; i < m_EnemiesInRound.Count; i++)
        {
            // Same as above, but for the enemy in this list
            if (m_EnemiesInRound[i] != null && m_EnemiesInRound[i].MatchFirstCharInHealth(input))
                // Return, because we want to process only the first reference
                return m_EnemiesInRound[i].gameObject;
        }

        // We didn't find anything
        return null;
    }

    /**
     * Loads the scene at the given index.
     **/
    public void LoadScene(int index)
    {
        // Load the scene at index
        SceneManager.LoadScene(index);
    }

    /**
     * Spawns a new enemy on the level with a given health, spawn position and index.
     **/
    public void SpawnEnemy(GameObject prefabToInstantiate, string health, Vector3 spawnPosition, Quaternion rotation,
        bool fromSet = true)
    {
        // Instantiate our enemies
        GameObject enemyClone = Instantiate(prefabToInstantiate, spawnPosition, rotation, m_EnemiesParentTransform);

        // Cache our enemy's script
        Enemy enemyScript = enemyClone.GetComponent<Enemy>();

        // Set the enemy's index to what is on the round
        enemyScript.Index = m_EnemiesInRound.Count;

        // Pick the health of the enemy from our language set
        enemyScript.OriginalHealthText = health;
        enemyScript.HealthText = enemyScript.OriginalHealthText;

        // Set the enemy's health (as integer) to the length of the health text
        enemyScript.Health = enemyScript.HealthText.Length;

        // Get the enemy's chase component
        // Note: I chose to set the chase script rather than on the Enemy itself because it's more
        // efficient to grab the player GameObject once at the GameController rather than at the
        // start of each enemy
        Chase enemyChaseScript = enemyClone.GetComponent<Chase>();

        // If the enemy has a chase component, then set the target to the player
        if (enemyChaseScript != null)
            // Set the player as the target
            enemyClone.GetComponent<Chase>().Target = m_PlayerInputScript.transform;

        // Now we set this enemy as an enemy in the round
        m_EnemiesInRound.Add(enemyScript);

        // If this enemy didn't originally came from our set we need to add it to our win condition
        if (!fromSet)
        {
            m_EnemiesNeededToWin++;

            // Increment our EnemiesLeftInRound
            m_EnemiesLeftInRound++;
        }
    }
}
