﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    /**
     * This script is what handles all the UI interactions from the main menu. We only need it
     * to disable the Cursor.
     **/

    /**
     * This struct allows us to represent a char in a sprite
     **/
    [Serializable]
    public struct FontSpriteToCharacter
    {
        public char Name;
        public Sprite Image;
    }

    /**
     * Public variables
     **/
    // This is the variable that we have assigned in the editor so we have a set of chars corresponding
    // their sprites in our font
    public FontSpriteToCharacter[] m_FontSpritesToCharacters;
    // Each of these buttons, are the buttons of the save slots we want to update their text of
    public Button m_SaveSlot1Button;
    public Button m_SaveSlot2Button;
    public Button m_SaveSlot3Button;
    // Same thing as above but for the level select buttons
    public Button m_Level1SelectButton;
    public Button m_Level2SelectButton;
    public Button m_Level3SelectButton;
    // Each of these transforms hold all of the child input fields in the corresponding SetSlot panel that
    // we want to update
    public Transform m_SetSlot1Panel;
    public Transform m_SetSlot2Panel;
    public Transform m_SetSlot3Panel;

    // This is the default background music we want to play
    public AudioClip m_DefaultBGMClip;

    /**
     * Private variables
     **/
    // I decided to create some private variables that we use to load the Images in the children of the
    // buttons, so we don't have to do it every time the function below is called
    private Image[] m_SaveSlot1Images;
    private Image[] m_SaveSlot2Images;
    private Image[] m_SaveSlot3Images;
    // Same for the input fields
    private InputField[] m_SetSlot1InputFields;
    private InputField[] m_SetSlot2InputFields;
    private InputField[] m_SetSlot3InputFields;
    // Same thing as above but for the level select images
    public Image[] m_Level1SelectImages;
    public Image[] m_Level2SelectImages;
    public Image[] m_Level3SelectImages;
    // Internally create a dictionary of chars and corresponding sprites to use later
    private Dictionary<char, Sprite> m_DictionaryOfCharactersAndSprites;

    // Use this for initialization
    void Start()
    {
        // Hide and lock our cursor, if we are not on the editor
#if UNITY_EDITOR
        Cursor.lockState = CursorLockMode.None;
#else
        Cursor.lockState = CursorLockMode.Locked;
#endif
        Cursor.visible = false;

        // Initialize our dictionary
        m_DictionaryOfCharactersAndSprites = new Dictionary<char, Sprite>();

        // Loop our m_FontSpritesToCharacters to build our dictionary
        for (int i = 0; i < m_FontSpritesToCharacters.Length; i++)
        {
            // Add it to the dictionary as a new entry
            m_DictionaryOfCharactersAndSprites.Add(m_FontSpritesToCharacters[i].Name, m_FontSpritesToCharacters[i].Image);
        }

        // Load the images from the children of the buttons
        m_SaveSlot1Images = m_SaveSlot1Button.GetComponentsInChildren<Image>();
        m_SaveSlot2Images = m_SaveSlot2Button.GetComponentsInChildren<Image>();
        m_SaveSlot3Images = m_SaveSlot3Button.GetComponentsInChildren<Image>();
        // Load the images for the select buttons of the level select
        m_Level1SelectImages = m_Level1SelectButton.GetComponentsInChildren<Image>();
        m_Level2SelectImages = m_Level2SelectButton.GetComponentsInChildren<Image>();
        m_Level3SelectImages = m_Level3SelectButton.GetComponentsInChildren<Image>();
        // Same for the input fields
        m_SetSlot1InputFields = m_SetSlot1Panel.GetComponentsInChildren<InputField>();
        m_SetSlot2InputFields = m_SetSlot2Panel.GetComponentsInChildren<InputField>();
        m_SetSlot3InputFields = m_SetSlot3Panel.GetComponentsInChildren<InputField>();

        // Ask our file manager to load sets
        AskFileManagerToLoadSets();

        // Tell our SoundManager to play the default BGM
        SoundManager.Instance.PlayBGM(m_DefaultBGMClip);
    }

    /**
     * This function updates the images that display the set title when we open the editor window.
     **/
    private void UpdateImagesToMatchSetDataTitle(SetData setData, Image[] imagesToUpdate)
    {
        // Now loop each letter of the loaded setData and see if the set slot images are matching
        for (int i = 0; i < imagesToUpdate.Length; i++)
        {
            // If we want to place a white space, then we need to set the sprite and alpha to null
            // Or if we are in an index bigger than our title length
            if (i >= setData.Title.Length || Char.IsWhiteSpace(setData.Title[i]))
            {
                imagesToUpdate[i].sprite = null;
                // Note: We have to store it in a temporary variable because of how Color works
                Color newColor = imagesToUpdate[i].color;
                newColor.a = 0;
                imagesToUpdate[i].color = newColor;

                // Skip the rest of the code
                continue;
            }

            // Upper case our char
            char letterOfSetDataName = Char.ToUpper(setData.Title[i]);

            // If the sprite is currently empty, then set the sprite to whatever we want and the alpha to 255
            if (imagesToUpdate[i].sprite == null)
            {
                // No need to match, it was an empty space before
                imagesToUpdate[i].sprite = m_DictionaryOfCharactersAndSprites[letterOfSetDataName];
                // Note: We have to store it in a temporary variable because of how Color works
                Color newColor = imagesToUpdate[i].color;
                newColor.a = 255;
                imagesToUpdate[i].color = newColor;
            }
            else if (!m_DictionaryOfCharactersAndSprites[letterOfSetDataName].name.Equals(imagesToUpdate[i].sprite.name))
                // We can see if it's matching by looking at the sprite file names and comparing it
                // to the char repesentative in our dictionaryOfCharactersAndSprites
                imagesToUpdate[i].sprite = m_DictionaryOfCharactersAndSprites[letterOfSetDataName];
        }
    }

    /**
     * This function updates the contents of the set slot panel (including the title).
     **/
    private void UpdateInputFieldsToMatchSetDataContents(SetData setData, InputField[] inputFieldsToUpdate)
    {
        // The first input field is always the title, so set that right away
        inputFieldsToUpdate[0].text = setData.Title;

        // Afterwards, look each of the contents and set them properly
        for (int i = 1; i < inputFieldsToUpdate.Length; i++)
        {
            // Grab the set data at this index
            string contentAtIndex = setData.Contents[i - 1];

            // If the set data we loaded is empty at this spot, skip it
            if (String.IsNullOrEmpty(contentAtIndex))
                continue;
            else
                // If it's not then set it
                inputFieldsToUpdate[i].text = contentAtIndex;
        }
    }

    /**
     * This coroutine waits for when an audio clip has finished playing, and then
     * loads a scene at the given index.
     **/
    private IEnumerator LoadLevelForAudio()
    {
        // Wait until our SFX source has finished
        yield return new WaitUntil(() => SoundManager.Instance.m_SFXSource.isPlaying == false);
        // Load our level
        SceneManager.LoadScene(1);
    }

    /**
     * This function tells our file manager to load the sets data we stored in the player's disk,
     * and then updates the various UI as necessary.
     **/
    public void AskFileManagerToLoadSets()
    {
        // Load sets if there are any
        SetData[] setsData = FileManager.Instance.LoadSets();

        // If we don't have any sets data, then don't load
        if (setsData == null)
        {
            return;
        }

        // Now for all 3 set slots, change their names to match what we loaded
        for (int i = 0; i < setsData.Length; i++)
        {
            // If we don't have anything in this position, skip it
            if (setsData[i] == null)
                continue;

            // Depending on the iteration we want to loop different slots
            switch (setsData[i].Index)
            {
                case 0:
                    // Update our save slot 1
                    UpdateImagesToMatchSetDataTitle(setsData[i], m_SaveSlot1Images);
                    // Update our level 1 select images
                    UpdateImagesToMatchSetDataTitle(setsData[i], m_Level1SelectImages);
                    // Update our set slot 1
                    UpdateInputFieldsToMatchSetDataContents(setsData[i], m_SetSlot1InputFields);
                    break;
                case 1:
                    // Update our save slot 2
                    UpdateImagesToMatchSetDataTitle(setsData[i], m_SaveSlot2Images);
                    // Update our level 2 select images
                    UpdateImagesToMatchSetDataTitle(setsData[i], m_Level2SelectImages);
                    // Update our set slot 2
                    UpdateInputFieldsToMatchSetDataContents(setsData[i], m_SetSlot2InputFields);
                    break;
                case 2:
                    // Update our save slot 3
                    UpdateImagesToMatchSetDataTitle(setsData[i], m_SaveSlot3Images);
                    // Update our level 3 select images
                    UpdateImagesToMatchSetDataTitle(setsData[i], m_Level3SelectImages);
                    // Update our set slot 3
                    UpdateInputFieldsToMatchSetDataContents(setsData[i], m_SetSlot3InputFields);
                    break;
            }
        }
    }

    /**
     * This function is called when we delete a set so we reset the save slot name as well as the
     * set slot's contents.
     **/
    public void ResetSet(int index)
    {
        // Depending on the index, target a different save slot, set slot and select images
        Image[] targetSaveSlot;
        Image[] targetSelectImages;
        InputField[] targetSetSlot;
        switch (index)
        {
            case 0:
                targetSaveSlot = m_SaveSlot1Images;
                targetSetSlot = m_SetSlot1InputFields;
                targetSelectImages = m_Level1SelectImages;
                break;
            case 1:
                targetSaveSlot = m_SaveSlot2Images;
                targetSetSlot = m_SetSlot2InputFields;
                targetSelectImages = m_Level2SelectImages;
                break;
            case 2:
                targetSaveSlot = m_SaveSlot3Images;
                targetSetSlot = m_SetSlot3InputFields;
                targetSelectImages = m_Level3SelectImages;
                break;
            default:
                return;
        }

        // Create a new color object which is white with no alpha
        Color noAlpha = new Color(255, 255, 255, 0);
        // Create a new color object which is white with 255 alpha
        Color fullAlpha = new Color(255, 255, 255, 255);

        // Reset this save slot to "NEW"
        for (int i = 0; i < targetSaveSlot.Length; i++)
        {
            // Set the first letter to N
            if (i == 0)
            {
                targetSaveSlot[i].sprite = m_DictionaryOfCharactersAndSprites['N'];
                targetSaveSlot[i].color = fullAlpha;
            }
            else if (i == 1)
            {
                // The second letter to E
                targetSaveSlot[i].sprite = m_DictionaryOfCharactersAndSprites['E'];
                targetSaveSlot[i].color = fullAlpha;
            }
            else if (i == 2)
            {
                // The third letter to W
                targetSaveSlot[i].sprite = m_DictionaryOfCharactersAndSprites['W'];
                targetSaveSlot[i].color = fullAlpha;
            }
            else
            {
                // Everything else to empty
                targetSaveSlot[i].sprite = null;
                targetSaveSlot[i].color = noAlpha;
            }
        }

        // Empty all the input fields
        for (int i = 0; i < targetSetSlot.Length; i++)
        {
            // Empty them
            targetSetSlot[i].text = "";
        }

        // Reset the select images as well
        for (int i = 0; i < targetSelectImages.Length; i++)
        {
            // Set the first letter to E
            if (i == 0)
            {
                targetSelectImages[i].sprite = m_DictionaryOfCharactersAndSprites['E'];
                targetSelectImages[i].color = fullAlpha;
            }
            else if (i == 1)
            {
                // The second letter to M
                targetSelectImages[i].sprite = m_DictionaryOfCharactersAndSprites['M'];
                targetSelectImages[i].color = fullAlpha;
            }
            else if (i == 2)
            {
                // The third letter to P
                targetSelectImages[i].sprite = m_DictionaryOfCharactersAndSprites['P'];
                targetSelectImages[i].color = fullAlpha;
            }
            else if (i == 3)
            {
                // The fourth letter to T
                targetSelectImages[i].sprite = m_DictionaryOfCharactersAndSprites['T'];
                targetSelectImages[i].color = fullAlpha;
            }
            else if (i == 4)
            {
                // The fifth letter to Y
                targetSelectImages[i].sprite = m_DictionaryOfCharactersAndSprites['Y'];
                targetSelectImages[i].color = fullAlpha;
            }
            else
            {
                // Everything else to empty
                targetSelectImages[i].sprite = null;
                targetSelectImages[i].color = noAlpha;
            }
        }


        // Tell our FileManager to delete this set as well
        FileManager.Instance.DeleteSet(index);
    }

    /**
     * This function sets the active set data and loads our level.
     **/
    public void SetActiveSetDataAndLoadScene(int activeSetDataIndex)
    {
        // If this set data doesn't exist, do nothing
        if (!FileManager.Instance.SetDataExistsAtIndex(activeSetDataIndex))
            return;

        // Set our active set data
        FileManager.Instance.ActiveSetDataIndex = activeSetDataIndex;

        // At the same time we want to load the scene after the clip has finished playing, so we
        // launch a coroutine which waits until the audio has finished playing and then loads the scene
        StartCoroutine(LoadLevelForAudio());
    }

    /**
     * This function is triggered by the Credits button onClick event so that we ask the sound manager
     * to play this BGM.
     * We can't add the SoundManager to the onClick event because it's a DontDestroyOnLoad GameObject.
     **/
    public void AskSoundManagerToPlayBGM(AudioClip clip)
    {
        SoundManager.Instance.PlayBGM(clip);
    }
}
