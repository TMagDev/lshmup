﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Word : Enemy
{
    /**     * This represents the enemy's who consist of one word. These enemies move faster then the "Letter" enemy and randomly     * shoot projectiles towards the player.     * I chose not to split this in to different classes  as I did with the player (such as EnemyMovement + BasicMovement,      * EnemyHealth + BasicHealth) because the enemy's behavior is not sufficiently complex.
     **/

    /**
     * Public variables
     **/

    /**
     * Private variables
     **/

    // Use this for initialization
    protected override void Start()
    {
        // Call the parent's Start
        base.Start();

        // Set how many points this Enemy is worth
        PointsWorth = 10;

        // Setup our delay and shoot rate
        m_ShootDelay = 1f;
        m_ShootRate = 5f;

        // Initialize our PlayerShotPooler
        m_ShotPooler = new GameObjectPooler(m_ShotPrefab, GameController.m_EffectsParentTransform);

        // We invoke repeating our implementation of Shoot
        InvokeRepeating("Shoot", m_ShootDelay, m_ShootRate);
    }

    /**
     * We override our inherited Shoot() function completely to implement our own version
     * of firing.
     **/
    protected override void Shoot()
    {
        // Calculate in which direction you want the shot to rotate towards
        Quaternion direction = Quaternion.LookRotation(transform.forward, m_ShotTarget.transform.position - transform.position);

        // Get the next available PlayerShot from our pool
        GameObject wordShotClone = m_ShotPooler.GetOrCreateAvailableGameObject();

        // If there is an available PlayerShot we can use, let's reset some data
        wordShotClone.transform.position = m_ShotSpawn[0].position;
        wordShotClone.transform.rotation = direction;

        // Set the target of the Chase script of our instantiated shot to our target
        wordShotClone.GetComponent<Chase>().Target = m_ShotTarget.transform;
    }
}
