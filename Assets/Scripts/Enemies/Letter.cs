﻿public class Letter : Enemy{
    /**     * This represents the enemy's who consist of one letter. These enemies do nothing but slowly move     * towards the player.     * I chose not to split this in to different classes  as I did with the player (such as EnemyMovement + BasicMovement,      * EnemyHealth + BasicHealth) because the enemy's behavior is not sufficiently complex.
     **/

    // Use this for initialization
    protected override void Start()
    {
        // Call the parent's Start
        base.Start();

        // Set how many points this Enemy is worth
        PointsWorth = 5;
    }}