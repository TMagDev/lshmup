﻿using System;
using System.Linq;
using UnityEngine;

public class Sentence : Enemy
{
    /**
     * This script handles the logic of the Sentence enemy. The Sentence enemy is slightly different
     * than others since it shoots Letter enemies, and moves down slightly and then left and right
     * constantly.
     **/

    /**
     * Public variables
     **/
    // Our own custom audio clip to play when we spawn
    public AudioClip m_SentenceClip;

    /**
     * Private variables
     **/
    // The various states this enemy can be is an enums
    private enum State
    {
        Move,
        Shoot,
        Patrol,
    };
    // The state that this enemy is in
    private State m_SentenceState;
    // The direction we are gonna patrol to
    private int m_SentencePatrolDirection;

    // Use this for initialization
    protected override void Start()
    {
        // Call the parent's Start
        base.Start();

        // Set how many points this Enemy is worth
        PointsWorth = 50;

        // Setup our delay and shoot rate
        m_ShootDelay = 1f;
        m_ShootRate = 5f;

        // Set our state to moving
        m_SentenceState = State.Move;

        // Set the direction to 1
        m_SentencePatrolDirection = 1;

        // Set the background music to our own customized version
        SoundManager.Instance.PlayBGM(m_SentenceClip);
    }

    /**
     * Called every frame
     **/
    void Update()
    {
        // We could use a state machine or something like that, but this enemy's state behavior
        // is much more simple, so no need to
        switch (m_SentenceState)
        {
            case State.Move:
                // Move towards where Y is equal to 3
                transform.position = Vector2.MoveTowards(transform.position, new Vector3(transform.position.x, 3f),
                    Time.deltaTime * 1.5f);

                // When Y is 3 we reached our target
                if (transform.position.y == 3f)
                    m_SentenceState = State.Shoot;
                break;
            case State.Shoot:
                // We invoke repeating our implementation of Shoot
                InvokeRepeating("Shoot", m_ShootDelay, m_ShootRate);

                // And immediately change our state to patrol
                m_SentenceState = State.Patrol;
                break;
            case State.Patrol:
                // Move towards where X is 4, and swap direction when we reached it
                transform.position = Vector2.MoveTowards(transform.position, new Vector3(4 * m_SentencePatrolDirection,
                    transform.position.y), Time.deltaTime * 1f);

                // When we reach our destination invert our direction
                if (transform.position.x == 4)
                    m_SentencePatrolDirection = -1;
                else if (transform.position.x == -4)
                    m_SentencePatrolDirection = 1;
                break;
        }
    }

    /**
     * We override our inherited Shoot() function completely to implement our own version
     * of firing.
     **/
    protected override void Shoot()
    {
        // Calculate in which direction you want the shot to rotate towards
        Quaternion direction = Quaternion.LookRotation(transform.forward, m_ShotTarget.transform.position - transform.position);

        // Split our health in to spaces
        string[] healthSplitByWhitespaces = m_HealthText.Trim().Split(null);

        // Randomly pick a health from the array of our sentences split in to words
        string randomHealth = "";

        // While our random health is empty, pick a random health
        while(String.IsNullOrEmpty(randomHealth) || randomHealth.Any(s => Char.IsWhiteSpace(s)))
        {
            // Pick a random health
            randomHealth = healthSplitByWhitespaces[UnityEngine.Random.Range(0, healthSplitByWhitespaces.Length - 1)];
        }

        // Choose a random spawn position
        Vector3 randomSpawnPosition = m_ShotSpawn[(int)UnityEngine.Random.value].transform.position;

        // Tell our game controller to spawn a new enemy
        m_GameControllerScript.SpawnEnemy(m_ShotPrefab, randomHealth, randomSpawnPosition, direction, false);
    }
}
