﻿using UnityEngine;
using UnityEngine.UI;

public abstract class Enemy : MonoBehaviour
{

    /**
     * This is the parent Enemy script where all enemies inherit from.
     * Here we have global behavior for enemies, such as their movement.
     **/

    /**
     * Public variables
     **/
    // The explosion we want to instantiate when this enemy is destroyed
    public GameObject m_EnemyExplosionPrefab;
    // The prefab we want to instantiate of our projectile
    public GameObject m_ShotPrefab;
    // Our weapon spawn point
    public Transform[] m_ShotSpawn;

    // The audio clip we want to play when we are destroyed
    public AudioClip m_ExplosionClip;

    /**
     * Private variables
     **/
    // Represents the enemy's health as an integer
    // Note: We have two representations of the enemy's health because one is for the player
    // to visually follow and type, while the other is to track when a player's shot projectile
    // hits our enemy, enabling us to destroy it on the last projectile hit
    protected int health;
    public int Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
        }
    }
    // Represents the enemy's original health so we can target this properly
    protected string m_OriginalHealthText;
    public string OriginalHealthText
    {
        get
        {
            return m_OriginalHealthText;
        }

        set
        {
            m_OriginalHealthText = value;
        }
    }
    // Represents the enemy's health as a visual string (text)
    protected string m_HealthText;
    public string HealthText
    {
        get
        {
            return m_HealthText;
        }

        set
        {
            m_HealthText = value;

            // Update our health text so we display the text, along the variable update
            m_HealthTextUI.text = HealthText;
        }
    }
    // Keep a reference to our GameController script
    protected GameController m_GameControllerScript;
    // The index in the GameController's array of enemies, this enemy belongs to
    protected int index;
    public int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }
    // How many points this enemy is worth, which should be implemented by the child classes
    protected int pointsWorth;
    public int PointsWorth
    {
        get
        {
            return pointsWorth;
        }

        set
        {
            pointsWorth = value;
        }
    }
    // We grab our HealthTextUI to update as well
    protected Text m_HealthTextUI;
    // How long we want to wait before we start firing
    protected float m_ShootDelay;
    // At what rate do we want to shoot
    protected float m_ShootRate;
    // Create a new GameObject pool for our shots
    protected GameObjectPooler m_ShotPooler;
    // Our target is always going to be the player
    protected GameObject m_ShotTarget;

    // Initialization references between scripts or object
    private void Awake()
    {
        // Get our text UI in our children
        // Note: That we have to do in the Awake function because if we do it in Start
        // it's already too late and the GameController is already trying to set the health for something
        // that isn't initalized yet
        m_HealthTextUI = GetComponentInChildren<Text>();
    }

    // Use this for initialization
    protected virtual void Start()
    {
        // Get our GameController script, in our GameController game object
        m_GameControllerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        // Find our player
        m_ShotTarget = GameObject.FindGameObjectWithTag("Player");
    }

    /**
     * Each enemy may or may not have it's own implementation of Shooting.
     * I left this function empty because enemies aren't obligated to shoot, they should be
     * responsible to implement their own version of it.
     **/
    protected virtual void Shoot() { }

    /**
     * This function looks at the char passed and checks whether it matches the
     * first character of this Enemy's health.
     **/
    public bool MatchFirstCharInHealth(char comparison)
    {
        // If we have any health text, and the first char in our string matches with comparison
        if (HealthText.Length > 0 && HealthText[0].Equals(comparison))
        {
            // Return true
            return true;
        }

        // Else return false
        return false;
    }

    /**
     * This function simply encapsulates the logic of removing the first char from the enemy's
     * health as text.
     **/
    public void RemoveCharFromHealthText()
    {
        // Remove this first char
        HealthText = m_HealthText.Substring(1, m_HealthText.Length - 1);

        // If we are dead, then destroy the parent of our text (which should be a Panel holding the text)
        if (IsHealthTextDead())
            // Then we destroy our parent game object
            Destroy(m_HealthTextUI.transform.parent.gameObject);
    }

    /**
     * When we are damage, we lose the first character in our health.
     * If we are destroyed (we have less than 0 characters), we tell our GameController.
     **/
    public virtual void Damage()
    {
        // Decrement our health
        Health--;

        // Check if we died, by checking if our Health as a value is empty
        if (Health <= 0)
        {
            // Play our explosion clip
            SoundManager.Instance.PlaySingle(m_ExplosionClip);

            // Because we died, trigger our death explosion
            Instantiate(m_EnemyExplosionPrefab, transform.position, Quaternion.identity, GameController.m_EffectsParentTransform);

            // Tell our GameController we died
            m_GameControllerScript.EnemyIsDead(Index);

            // Destroy our pool if we have one
            if (m_ShotPooler != null)
                // Destroy our pool
                m_ShotPooler.DestroyPool();
        }
    }

    /**
     * This simple function checks if our health text is empty (therefor, dead).
     **/
    public bool IsHealthTextDead()
    {
        return HealthText.Length <= 0;
    }
}
