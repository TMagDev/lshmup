﻿using System;
using UnityEngine;

[Serializable]
public class SetData
{

    /**
     * This is the class we want to serialize set data with the file manager.
     **/

    /**
     * Private variables
     **/
    // The index of the save slot we want to save/load to
    [SerializeField]
    private int index;
    public int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }
    // The highscore the player has achieved for this level
    [SerializeField]
    private int highscore;
    public int Highscore
    {
        get
        {
            return highscore;
        }

        set
        {
            highscore = value;
        }
    }
    // This is the name of the set
    [SerializeField]
    private string title;
    public string Title
    {
        get
        {
            return title;
        }

        set
        {
            title = value;
        }
    }
    // The contents of the set
    [SerializeField]
    private string[] contents;
    public string[] Contents
    {
        get
        {
            return contents;
        }

        set
        {
            contents = value;
        }
    }

    // Constructor
    public SetData(int newIndex, string newName, string[] newContents, int newHighscore)
    {
        Index = newIndex;
        Title = newName;
        Contents = newContents;
        Highscore = newHighscore;
    }
}
